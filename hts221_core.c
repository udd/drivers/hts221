/*
 * device.c
 *
 *  Created on: 22 Oct 2020
 *  Author: dbaret
 */
#include "hts221_core.h"


/**
  * Get configuration registers from the sensor
  *
  * @param  comm    communication struct defined from the user (ptr)
  * @param  config  configuration struct where to write configuration registers (ptr)
  *
**/
void
get_config_registers (struct hts221_communication *comm,
		      struct hts221_configuration *config)
{
  uint8_t reg;
  comm->read (HTS221_AV_CONF, &reg);

  config->avgh = reg & 0x07;
  config->avgt = (reg >> 3) & 0x07;

  //CTRL 1
  comm->read (HTS221_CTRL_REG1, &reg);

  config->odr = reg & 0x03;
  config->bdu = (reg >> 2) & 0x01;
  config->pd = (reg >> 7) & 0x01;

  //CTRL 2
  comm->read (HTS221_CTRL_REG2, &reg);

  config->one_shot = reg & 0x01;
  config->heater = (reg >> 1) & 0x01;
  config->boot = (reg >> 7) & 0x01;


  //CTRL 3
  comm->read (HTS221_CTRL_REG3, &reg);

  config->drdy = (reg >> 2) & 0x01;
  config->pp_od = (reg >> 6) & 0x01;
  config->drdy_h_l = (reg >> 7) & 0x01;
}

/**
  * Set user configuration in the sensor
  *
  * @param  comm    communication struct defined from the user (ptr)
  * @param  config  configuration struct where the configurations are saved (ptr)
  *
**/
void
set_config_registers (struct hts221_communication *comm,
		      struct hts221_configuration *config)
{
  uint8_t reg;
  comm->read (HTS221_AV_CONF, &reg);

  reg &= AV_CONF_RES_BIT;

  reg |= config->avgh;
  reg |= (config->avgt << 3);

  comm->write (HTS221_AV_CONF, &reg);


  // CTRL 1
  comm->read (HTS221_CTRL_REG1, &reg);

  reg &= CTRL_REG_1_RES_BIT;

  reg |= config->odr;
  reg |= (config->bdu << 2);
  reg |= (config->pd << 7);

  comm->write (HTS221_CTRL_REG1, &reg);

  //CTRL 2
  comm->read (HTS221_CTRL_REG2, &reg);

  reg &= CTRL_REG_2_RES_BIT;

  reg |= config->one_shot;
  reg |= (config->heater << 1);
  reg |= (config->boot << 7);

  comm->write (HTS221_CTRL_REG2, &reg);

  //CTRL 3
  comm->read (HTS221_CTRL_REG3, &reg);

  reg &= CTRL_REG_3_RES_BIT;

  reg |= (config->drdy << 2);
  reg |= (config->pp_od << 6);
  reg |= (config->drdy_h_l << 7);

  comm->write (HTS221_CTRL_REG3, &reg);
}

/**
  * Get temperature raw data
  *
  * @param  comm      communication struct defined from the user (ptr)
  * @param  raw_data  int16 value where the data are saved (ptr)
  *
**/
void
get_temp_raw_data (struct hts221_communication *comm, int32_t * raw_data)
{
  uint8_t data = 0;

  comm->read (HTS221_TEMP_OUT_L, &data);
  *raw_data = data;

  comm->read (HTS221_TEMP_OUT_H, &data);
  *raw_data |= (data << 8);
}

/**
  * Get humidity raw data
  *
  * @param  comm      communication struct defined from the user (ptr)
  * @param  raw_data  int16 value where the data are saved (ptr)
  *
**/
void
get_hum_raw_data (struct hts221_communication *comm, int16_t * raw_data)
{
  uint8_t data = 0;

  comm->read (HTS221_HUMIDITY_OUT_L, &data);
  *raw_data = data;

  comm->read (HTS221_HUMIDITY_OUT_H, &data);
  *raw_data |= (data << 8);
}

/**
  * Get calibration data
  *
  * @param  comm        communication struct defined from the user (ptr)
  * @param  calib_data  calibration struct where to write the data (ptr)
  *
**/
void
get_calibration_data (struct hts221_communication *comm,
		      struct calib_data *calib)
{
  uint8_t data_read[2];

  // TEMPERATURE
  // T0
  hts221_read_multiple_registers (HTS221_T0_OUT_LSB, data_read, 2);
  calib->t0_out = data_read[0] + (data_read[1] << 8);

  // T1
  hts221_read_multiple_registers (HTS221_T1_OUT_LSB, data_read, 2);
  calib->t1_out = data_read[0] + (data_read[1] << 8);

  // T0 deg
  comm->read (HTS221_T0_degC_x8, data_read);
  comm->read (HTS221_T0_T1_MSB, data_read + 1);
  calib->t0_degC_x8 = data_read[0] + ((data_read[1] & 0x03) << 8);

  // T1 deg
  comm->read (HTS221_T1_degC_x8, data_read);
  comm->read (HTS221_T0_T1_MSB, data_read + 1);
  calib->t1_degC_x8 = data_read[0] + (((data_read[1] >> 2) & 0x03) << 8);

  // HUMIDITY
  // H0
  hts221_read_multiple_registers (HTS221_H0_OUT_LSB, data_read, 2);
  calib->h0_t0_out = data_read[0] + (data_read[1] << 8);

  // H1
  hts221_read_multiple_registers (HTS221_H1_OUT_LSB, data_read, 2);
  calib->h1_t0_out = data_read[0] + (data_read[1] << 8);

  // H0 rH
  comm->read (HTS221_H0_rH_x2, data_read);
  calib->h0_rH_x2 = data_read[0];

  // H1 rH
  comm->read (HTS221_H1_rH_x2, data_read);
  calib->h1_rH_x2 = data_read[0];
}
