#!/bin/bash

# echo empty line
printf "\n\n"

return_value=0

# iterate over all *.c and *.h files to check if they are formatted correctly
while read file
do
	echo -n "Processing $file"
	indent $file
	if [[ $(diff $file~ $file) ]]
	then
		echo " -> not formatted correctly"
		return_value=1
	else
		echo " -> formatted correctly"
	fi
done < <(find . -name '*.c' -o -name '*.h')

# echo empty line
printf "\n\n"

exit $return_value
