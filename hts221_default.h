/*
 * hts221_default.h
 *
 *  Created on: Feb 13, 2021
 *  Author: dbaret
 */

#ifndef HTS221_HTS221_DEFAULT_H_
#define HTS221_HTS221_DEFAULT_H_

#include "hts221.h"

/*
 * Temperature resolution
 *
 * Values:
 * 		HTS221_AVGT_2		Temperature resolution => 0.08
 *		HTS221_AVGT_4		Temperature resolution => 0.05
 *		HTS221_AVGT_8		Temperature resolution => 0.04
 *		HTS221_AVGT_16		Temperature resolution => 0.03
 *		HTS221_AVGT_32		Temperature resolution => 0.02
 *		HTS221_AVGT_64		Temperature resolution => 0.015
 *		HTS221_AVGT_128		Temperature resolution => 0.01
 *		HTS221_AVGT_256		Temperature resolution => 0.007
 */
#define HTS221_AVGT_DEF		HTS221_AVGT_128

/*
 * Humidity resolution
 *
 * Values:
 *		HTS221_AVGH_4		Humidity resolution => 0.4
 *		HTS221_AVGH_8		Humidity resolution => 0.3
 *		HTS221_AVGH_16		Humidity resolution => 0.2
 *		HTS221_AVGH_32		Humidity resolution => 0.15
 *		HTS221_AVGH_64		Humidity resolution => 0.1
 *		HTS221_AVGH_128		Humidity resolution => 0.07
 *		HTS221_AVGH_256		Humidity resolution => 0.05
 *		HTS221_AVGH_512		Humidity resolution => 0.03
 */
#define HTS221_AVGH_DEF		HTS221_AVGH_32

/*
 * Power mode
 *
 * Values:
 * 		HTS221_DISABLE		Set sensor in power down mode
 * 		HTS221_ENABLE		Set sensor in active mode
 */
#define HTS221_PD_DEF		HTS221_ENABLE

/*
 * Block data update
 *
 * Values:
 * 		HTS221_DISABLE		Continuous update
 * 		HTS221_ENABLE		Output registers not updated until MSB and LSB reading
 */
#define HTS221_BDU_DEF		HTS221_ENABLE

/*
 * Output data rate
 *
 * Values:
 * 		HTS221_ONE_SHOT		Output data rate => One-shot
 *		HTS221_ODR_1Hz		Output data rate => 1 Hz
 *		HTS221_ODR_7Hz		Output data rate => 7 Hz
 *		HTS221_ODR_12Hz5	Output data rate => 12.5 Hz
 */
#define HTS221_ODR_DEF		HTS221_ODR_1Hz

/*
 * Reboot memory content
 *
 * Values:
 * 		HTS221_DISABLE		Normal mode
 * 		HTS221_ENABLE		Reboot memory content
 */
#define HTS221_BOOT_DEF		HTS221_DISABLE

/*
 * Heater
 *
 * Values:
 * 		HTS221_DISABLE		Heater disable
 * 		HTS221_ENABLE		Heater enable
 */
#define HTS221_HEATER_DEF	HTS221_DISABLE

/*
 * One-shot
 *
 * Values:
 * 		HTS221_DISABLE		Waiting for start of conversion
 * 		HTS221_ENABLE		Start for a new dataset
 */
#define HTS221_ONE_SHOT_DEF	HTS221_DISABLE

/*
 * Data Ready output signal active high, low
 *
 * Values:
 * 		HTS221_DISABLE		Active high
 * 		HTS221_ENABLE		Active low
 */
#define HTS221_DRDY_H_L_DEF	HTS221_DISABLE

/*
 * Push-pull / Open Drain selection
 *
 * Values:
 * 		HTS221_DISABLE		Push-pull
 * 		HTS221_ENABLE		Open drain
 */
#define HTS221_PP_OD_DEF	HTS221_DISABLE

/*
 * Data Ready enable
 *
 * Values:
 * 		HTS221_DISABLE		Data Ready disabled
 * 		HTS221_ENABLE		Data Ready enabled
 */
#define HTS221_DRDY_DEF		HTS221_DISABLE

#endif /* HTS221_HTS221_DEFAULT_H_ */
