/*
 *  hts221.h
 *
 *  Created on: 15 Oct 2020
 *  Author: dbaret
 */

#ifndef HTS221_DRIVER_HTS221_H_
#define HTS221_DRIVER_HTS221_H_

#include <stdint.h>		//may not be ideal depending on linking of implementation, maybe add as requirement?

/*! \addtogroup hts221_defines
*  HTS221 defines
*  @{
*/

/* Read and write */
#define HTS221_READ_ADDRESS     0xBF	/**< Read address **/
#define HTS221_WRITE_ADDRESS    0xBE	/**< Write address **/

/*! @} */

/*! \addtogroup hts221_registers
*  HTS221 registers
*  @{
*/

/*********** REGISTERS ***********/

/* Setup registers */
#define HTS221_AV_CONF          0x10	/**< Humidity and temperature resolution register **/
#define HTS221_CTRL_REG1        0x20	/**< Control register 1 **/
#define HTS221_CTRL_REG2        0x21	/**< Control register 2 **/
#define HTS221_CTRL_REG3        0x22	/**< Control register 3 **/

/* Data registers */
#define HTS221_WHO_AM_I         0x0F	/**< Device identification register **/
#define HTS221_STATUS_REG       0x27	/**< Status register **/
#define HTS221_HUMIDITY_OUT_L   0x28	/**< Relative humidity data (LSB) **/
#define HTS221_HUMIDITY_OUT_H   0x29	/**< Relative humidity data (MSB) **/
#define HTS221_TEMP_OUT_L       0x2A	/**< Temperature data (LSB) **/
#define HTS221_TEMP_OUT_H       0x2B	/**< Temperature data (MSB) ***/

/* Calibration registers **/
#define HTS221_T0_degC_x8       0x32	/**< Temperature calibration register (Deg) **/
#define HTS221_T1_degC_x8       0x33	/**< Temperature calibration register (Deg) **/
#define HTS221_T0_T1_MSB        0x35	/**< Temperature calibration register (Deg) **/

#define HTS221_H0_rH_x2         0x30	/**< Humidity calibration register (rH) **/
#define HTS221_H1_rH_x2         0x31	/**< Humidity calibration register (rH) **/

#define HTS221_T0_OUT_LSB       0x3C	/**< Temperature calibration register (ADC) **/
#define HTS221_T0_OUT_MSB       0x3D	/**< Temperature calibration register (ADC) **/
#define HTS221_T1_OUT_LSB       0x3E	/**< Temperature calibration register (ADC) **/
#define HTS221_T1_OUT_MSB       0x3F	/**< Temperature calibration register (ADC) **/

#define HTS221_H0_OUT_LSB       0x36	/**< Humidity calibration register (ADC) **/
#define HTS221_H0_OUT_MSB       0x37	/**< Humidity calibration register (ADC) **/
#define HTS221_H1_OUT_LSB       0x3A	/**< Humidity calibration register (ADC) **/
#define HTS221_H1_OUT_MSB       0x3B	/**< Humidity calibration register (ADC) **/


/* Reserved bit in registers */
#define AV_CONF_RES_BIT         0xC0	/**< AV reserved bit mask **/
#define CTRL_REG_1_RES_BIT      0x78	/**< CTRL 1 reserved bit mask **/
#define CTRL_REG_2_RES_BIT      0x7C	/**< CTRL 2 reserved bit mask **/
#define CTRL_REG_3_RES_BIT      0x3B	/**< CTRL 3 reserved bit mask **/


/*! @} */

/*! \addtogroup hts221_settings
*  HTS221 settings
*  @{
*/

/*********** SETTINGS DEFINITIONS ***********/
#define HTS221_ENABLE           0x01
#define HTS221_DISABLE          0x00


#define HTS221_AVGT_2           0x00	/**< Temperature resolution => 0.08 **/
#define HTS221_AVGT_4           0x01	/**< Temperature resolution => 0.05 **/
#define HTS221_AVGT_8           0x02	/**< Temperature resolution => 0.04 **/
#define HTS221_AVGT_16          0x03	/**< Temperature resolution => 0.03 **/
#define HTS221_AVGT_32          0x04	/**< Temperature resolution => 0.02 **/
#define HTS221_AVGT_64          0x05	/**< Temperature resolution => 0.015 **/
#define HTS221_AVGT_128         0x06	/**< Temperature resolution => 0.01 **/
#define HTS221_AVGT_256         0x07	/**< Temperature resolution => 0.007 **/


#define HTS221_AVGH_4           0x00	/**< Humidity resolution => 0.4 **/
#define HTS221_AVGH_8           0x01	/**< Humidity resolution => 0.3 **/
#define HTS221_AVGH_16          0x02	/**< Humidity resolution => 0.2 **/
#define HTS221_AVGH_32          0x03	/**< Humidity resolution => 0.15 **/
#define HTS221_AVGH_64          0x04	/**< Humidity resolution => 0.1 **/
#define HTS221_AVGH_128         0x05	/**< Humidity resolution => 0.07 **/
#define HTS221_AVGH_256         0x06	/**< Humidity resolution => 0.05 **/
#define HTS221_AVGH_512         0x07	/**< Humidity resolution => 0.03 **/


#define HTS221_ONE_SHOT         0x00	/**< Output data rate => One-shot **/
#define HTS221_ODR_1Hz          0x01	/**< Output data rate => 1 Hz **/
#define HTS221_ODR_7Hz          0x02	/**< Output data rate => 7 Hz **/
#define HTS221_ODR_12Hz5        0x03	/**< Output data rate => 12.5 Hz **/

/*! @} */

/*! \addtogroup hts221_structs
*  HTS221 structs
*  @{
*/

/*********** STRUCTS ***********/

struct hts221_configuration
{
  uint16_t avgt:3;
  uint16_t avgh:3;
  uint16_t pd:1;
  uint16_t bdu:1;
  uint16_t odr:2;
  uint16_t boot:1;
  uint16_t heater:1;
  uint16_t one_shot:1;
  uint16_t drdy_h_l:1;
  uint16_t pp_od:1;
  uint16_t drdy:1;
};

struct calib_data
{
  int16_t h0_rH_x2;
  int16_t h1_rH_x2;
  int16_t t0_degC_x8;
  int16_t t1_degC_x8;
  int16_t h0_t0_out;
  int16_t h1_t0_out;
  int16_t t0_out;
  int16_t t1_out;
};


struct hts221_communication
{
  void (*write) (uint8_t reg, uint8_t * buf);	      /**< User write function **/
  void (*read) (uint8_t reg, uint8_t * buf);	      /**< User read function **/
};

typedef struct
{
  struct hts221_communication comm;
  struct hts221_configuration config;
} hts221_t;

/*! @} */

/*! \addtogroup hts221_functions
*  HTS221 functions
*  @{
*/

/*********** Functions declaration ***********/

/**
  * Save the pointer of the write and read function
  * that the user defined. Also modify the config struct
  * of the user with the default variable.
  *
  * @param  configs   user defined setup struct (ptr)
  * @retval           interface status (MANDATORY: return 0 -> no Error) TODO
  *
**/
void hts221_setup_communication (hts221_t * configs);

/**
  * Copy the configuration variables from the
  * default configuration file.
  *
  * @param  configs   user defined setup struct (ptr)
  * @retval           interface status (MANDATORY: return 0 -> no Error) TODO
  *
**/
void hts221_default_settings (hts221_t * configs);

/**
  * Write the user configurations in the HTS221 sensor
  *
  * @param  configs   user defined setup struct (ptr)
  * @retval           interface status (MANDATORY: return 0 -> no Error) TODO
  *
**/
void hts221_setup_sensor (hts221_t * configs);

/**
  * Read multiple (consecutive) registers of the HTS221 sensor
  *
  * @param  hts221_register      register address
  * @param  read_data            pointer where readout should be stored to (ptr). Must have a size of at least \p size
  * @param  size                 amount of consecutive register to be read
  * @retval                      interface status (MANDATORY: return 0 -> no Error) TODO
  *
  */
void hts221_read_multiple_registers (uint8_t hts221_register,
				     uint8_t * read_data, uint8_t size);

/**
  * Get the interpolated temperature from HTS221
  *
  * @retval            The temperature in degrees
  *
**/
float hts221_get_temperature ();

/**
  * Get the interpolated humidity from HTS221
  *
  * @retval            The humidity in percent
  *
**/
float hts221_get_humidity ();

/**
  * Get the WHO AM I register
  *
  * @retval             WHO AM I value
  *
**/
uint8_t hts221_get_who_am_i (void);

/**
  * Interrupt handler. The interrupt flag in the HTS221
  * gets cleared if the temperature and the humidity are
  * read.
  *
  * @param  data   array of float where save the data(ptr)
  *
**/
void hts221_interrupt_handler (float *data);

#endif /* HTS221_DRIVER_HTS221_H_ */

/*! @} */
