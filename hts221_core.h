/*
 * device.h
 *
 *  Created on: 22 Oct 2020
 *  Author: dbaret
 */

#ifndef HTS221_HTS221_CORE_H_
#define HTS221_HTS221_CORE_H_

#include "hts221.h"

/*! \addtogroup hts221_core_functions
*  HTS221_CORE functions
*  @{
*/

/**
  * Get configuration registers from the sensor
  *
  * @param  comm    communication struct defined from the user (ptr)
  * @param  config  configuration struct where to write configuration registers (ptr)
  *
**/
void get_config_registers (struct hts221_communication *comm,
			   struct hts221_configuration *config);

/**
  * Set user configuration in the sensor
  *
  * @param  comm    communication struct defined from the user (ptr)
  * @param  config  configuration struct where the configurations are saved (ptr)
  *
**/
void set_config_registers (struct hts221_communication *comm,
			   struct hts221_configuration *config);

/**
  * Get temperature raw data
  *
  * @param  comm      communication struct defined from the user (ptr)
  * @param  raw_data  int16 value where the data are saved (ptr)
  *
**/
void get_temp_raw_data (struct hts221_communication *comm,
			int32_t * raw_data);

/**
  * Get humidity raw data
  *
  * @param  comm      communication struct defined from the user (ptr)
  * @param  raw_data  int16 value where the data are saved (ptr)
  *
**/
void get_hum_raw_data (struct hts221_communication *comm, int16_t * raw_data);

/**
  * Get calibration data
  *
  * @param  comm        communication struct defined from the user (ptr)
  * @param  calib_data  calibration struct where to write the data (ptr)
  *
**/
void get_calibration_data (struct hts221_communication *comm,
			   struct calib_data *calib);

/*! @} */

#endif /* HTS221_HTS221_CORE_H_ */
