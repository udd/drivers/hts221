/*
 ******************************************************************************
 * @file    hts221.c
 * @author  Universal Driver Development Team
 * @brief   HTS221 driver file
 ******************************************************************************
 * @attention
 *
 * Beautiful license
 *
 ******************************************************************************
 */


/*************** INCLUDE ***************/
#include "hts221.h"
#include "hts221_core.h"
#include "hts221_default.h"

/*************** GLOBAL VARIABLE ***************/
struct hts221_communication hts221_comm;
struct calib_data calib;


/**
  * Save the pointer of the write and read function
  * that the user defined. Also modify the config struct
  * of the user with the default variable.
  *
  * @param  setup   user defined setup struct (ptr)
  * @retval         interface status (MANDATORY: return 0 -> no Error) TODO
  *
  */
void
hts221_setup_communication (hts221_t * configs)
{

  /* Save pointer to function globally */
  hts221_comm.read = configs->comm.read;
  hts221_comm.write = configs->comm.write;

  /* Get and save globally the calibration data for temperature and humidity */
  get_calibration_data (&hts221_comm, &calib);

  // TODO: Implement return if successful
}

/**
  * Copy the configuration variables from the
  * default configuration file.
  *
  * @param  configs   user defined setup struct (ptr)
  * @retval           interface status (MANDATORY: return 0 -> no Error) TODO
  *
**/
void
hts221_default_settings (hts221_t * configs)
{

  /* Copy default settings */
  configs->config.avgt = HTS221_AVGT_DEF;
  configs->config.avgh = HTS221_AVGH_DEF;
  configs->config.pd = HTS221_PD_DEF;
  configs->config.bdu = HTS221_BDU_DEF;
  configs->config.odr = HTS221_ODR_DEF;
  configs->config.boot = HTS221_BOOT_DEF;
  configs->config.heater = HTS221_HEATER_DEF;
  configs->config.one_shot = HTS221_ONE_SHOT_DEF;
  configs->config.drdy_h_l = HTS221_DRDY_H_L_DEF;
  configs->config.pp_od = HTS221_PP_OD_DEF;
  configs->config.drdy = HTS221_DRDY_DEF;
}

/**
  * Write the user configurations in the HTS221 sensor
  *
  * @param  setup   user defined setup struct (ptr)
  * @retval         interface status (MANDATORY: return 0 -> no Error) TODO
  *
  */
void
hts221_setup_sensor (hts221_t * configs)
{

  /* Set user configuration */
  set_config_registers (&hts221_comm, &configs->config);

}


/**
  * Read multiple (consecutive) registers of the HTS221 sensor
  *
  * @param  hts221_register      register address
  * @param  read_data            pointer where readout should be stored to (ptr). Must have a size of at least \p size
  * @param  size                 amount of consecutive register to be read
  * @retval                      interface status (MANDATORY: return 0 -> no Error) TODO
  *
  */
__attribute__((weak))
     void hts221_read_multiple_registers (uint8_t hts221_register,
					  uint8_t * read_data, uint8_t size)
{

  for (uint8_t i = 0; i < size; ++i)
    {
      hts221_comm.read (hts221_register, read_data);
      ++read_data;
      ++hts221_register;
    }

}


/**
  * Get the interpolated temperature from HTS221
  *
  * @retval        The temperature in degrees
  *
  */
float
hts221_get_temperature ()
{

  int32_t raw_data = 0;

  /* Get temperature raw data */
  get_temp_raw_data (&hts221_comm, &raw_data);

  /* Interpolate with calibration data */
  int32_t result = (calib.t1_degC_x8 - calib.t0_degC_x8)
    * (raw_data - calib.t0_out) / (calib.t1_out - calib.t0_out)
    + calib.t0_degC_x8;

  /* Return temperature in degrees */
  return (float) result / 8.0f;
}

/**
  * Get the interpolated humidity from HTS221
  *
  * @retval        The humidity in percent
  *
  */
float
hts221_get_humidity ()
{

  int16_t raw_data = 0;

  /* Get humidity raw data */
  get_hum_raw_data (&hts221_comm, &raw_data);

  /* Interpolate with calibration data */
  int16_t result = (calib.h1_rH_x2 - calib.h0_rH_x2)
    * (raw_data - calib.h0_t0_out) / (calib.h1_t0_out - calib.h0_t0_out)
    + calib.h0_rH_x2;

  /* Return humidity in percent */
  return (float) result / 2.0f;

}

/**
  * Get the WHO AM I register
  *
  * @retval        WHO AM I value
  *
  */
uint8_t
hts221_get_who_am_i ()
{

  uint8_t who_am_i = 0;
  hts221_comm.read (HTS221_WHO_AM_I, &who_am_i);
  return who_am_i;

}


/**
  * Interrupt handler. The interrupt flag in the HTS221
  * gets cleared if the temperature and the humidity are
  * read.
  *
  * @param  data    array of float where save the data(ptr)
  *
  */
void
hts221_interrupt_handler (float *data)
{

  data[0] = hts221_get_temperature ();

  data[1] = hts221_get_humidity ();

}
